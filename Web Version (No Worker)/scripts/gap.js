var url = 'http://astarte.csr.unibo.it/gapdata/';
var request;

function httpRequest() {
    solBest = undefined;
    zBest = Number.MAX_VALUE;
    var value = document.getElementById("dropDown").value;
    if (value === "") {
        value = prompt("Insert the file name and path on \"http://astarte.csr.unibo.it/gapdata/\":", "");
    }
    request = new XMLHttpRequest();
    request.open('GET', url + value, true);
    request.onreadystatechange = handler;
    request.send();
}

function handler() {
    if (request.readyState == 4)
        if (request.status == 200)
            outputResult();
        else
            alert("Error\n   Status: " + request.status);
}

function outputResult() {
    var response = request.responseText;
    var textDiv = document.getElementById("textDiv");
    jInstance = JSON.parse(response);
    textDiv.innerHTML = syntaxHighlight(jInstance).replace(/(\r\n|\n|\r)/gm, "");
    setInstance(jInstance);
}

function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }

    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

function setInstance(jInstance) {
    m = jInstance.numfacilities;
    n = jInstance.numcustomers;
    c = jInstance.cost;
    req = jInstance.req;
    cap = jInstance.cap;
    alert("Obtained instance \"" + jInstance.name + "\"");
}

function callAndProcessResult(algorithm, parameters) {
    console.log("Calling " + algorithm.name + "(" + parameters + ")");
    var timer = prompt("Timeout (only integer, positive number):", "");
    timer = parseInt(timer);
    if (timer !== Number.NaN && timer > 0) {
        parameters.push(timer);
    }
    var result = algorithm.apply(this, parameters);

    printBestSolution(result.time, result.iteration);
}

function printBestSolution(time, iteration) {
    if (solBest != undefined && zBest != Number.MAX_VALUE) {
        zBest = checkFeasibility(solBest)
        alert("Current best solution: " + solBest + '\nCost: ' + zBest + "\nSolution is " +
            ((checkFeasibility(solBest) != Number.MAX_VALUE ? 'feasible' : 'not feasible') +
            (time != undefined ? "\nSolution obtained in " + time + " msec" : "") +
            (iteration != undefined ? "\nand " + iteration + " iterations" : "")));
    } else {
        alert('Solution was not found!');
    }
}

function instanceLoaded() {
    document.getElementById("constructive").className = document.getElementById("constructive").className.replace(" deactivated", '');
    document.getElementById("grasp").className = document.getElementById("grasp").className.replace(" deactivated", '');

    if (document.getElementById('show').className.indexOf('deactivated') == -1) {
        document.getElementById("show").className += " deactivated";
    }
    if (document.getElementById('feasibility').className.indexOf('deactivated') == -1) {
        document.getElementById("feasibility").className += " deactivated";
    }
    if (document.getElementById('opt10').className.indexOf('deactivated') == -1) {
        document.getElementById("opt10").className += " deactivated";
    }
    if (document.getElementById('opt11').className.indexOf('deactivated') == -1) {
        document.getElementById("opt11").className += " deactivated";
    }
    if (document.getElementById('simAnn').className.indexOf('deactivated') == -1) {
        document.getElementById("simAnn").className += " deactivated";
    }
    if (document.getElementById('tabuSearch').className.indexOf('deactivated') == -1) {
        document.getElementById("tabuSearch").className += " deactivated";
    }
    if (document.getElementById('ils').className.indexOf('deactivated') == -1) {
        document.getElementById("ils").className += " deactivated";
    }
    if (document.getElementById('vnd').className.indexOf('deactivated') == -1) {
        document.getElementById("vnd").className += " deactivated";
    }
    if (document.getElementById('vns').className.indexOf('deactivated') == -1) {
        document.getElementById("vns").className += " deactivated";
    }
}

function solutionComputed() {
    if(solBest != undefined) {
        document.getElementById("show").className =
            document.getElementById("show").className.replace(" deactivated", '');
        document.getElementById("feasibility").className =
            document.getElementById("feasibility").className.replace(" deactivated", '');
        document.getElementById("opt10").className =
            document.getElementById("opt10").className.replace(" deactivated", '');
        document.getElementById("opt11").className =
            document.getElementById("opt11").className.replace(" deactivated", '');
        document.getElementById("simAnn").className =
            document.getElementById("simAnn").className.replace(" deactivated", '');
        document.getElementById("tabuSearch").className =
            document.getElementById("tabuSearch").className.replace(" deactivated", '');
        document.getElementById("ils").className =
            document.getElementById("ils").className.replace(" deactivated", '');
        document.getElementById("vnd").className =
            document.getElementById("vnd").className.replace(" deactivated", '');
        document.getElementById("vns").className =
            document.getElementById("vns").className.replace(" deactivated", '');
    }
}

function iterationValue() {
    var content = parseInt(document.getElementById("iterationNumber").value);
    if (content != Number.NaN && content > 0) {
        return content;
    }
    return undefined;
}