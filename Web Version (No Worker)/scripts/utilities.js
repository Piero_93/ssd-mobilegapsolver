/**
 * Check the feasibility of a solution.
 * @param the solution
 * @return {number} the cost of the solution if it is feasible, else +infinity
 */
function checkFeasibility(solution) {
    var z = 0;
    var capUsed = new Array(m);

    for (var i = 0; i < m; i++) {
        capUsed[i] = 0;
    }

    //Controllo degli assegnamenti
    for (var j = 0; j < n; j++) {
        if (solution[j] < 0 || solution[j] >= m || solution[j] === undefined) { //Not feasible (infinity)
            return Number.MAX_VALUE;
        } else {
            z += c[solution[j]][j];
        }
    }

    for (j = 0; j < n; j++) {
        capUsed[solution[j]] += req[solution[j]][j];
        if (capUsed[solution[j]] > cap[solution[j]]) {
            return Number.MAX_VALUE;
        }
    }
    return z;
}

/**
 * Clone an array
 * @param arr array to be cloned
 * @returns {*} cloned array
 */
function arrayClone(array) {
    var copy;

    if (Array.isArray(array)) {
        copy = array.slice(0);
        for (var i = 0; i < copy.length; i++) {
            copy[i] = arrayClone(copy[i]);
        }
        return copy;
    } else if (typeof array === 'object') {
        throw 'Cannot clone array containing an object!';
    } else {
        return array;
    }
}

/**
 * Compute remaining capacity for a solution
 * @param solution the solution
 * @returns {[*]} remaining capacity
 */
function computeCapLeft(solution) {
    var capLeft = [m];
    for (var i = 0; i < m; i++) { //Inizializzo il vettore capLeft con le capacità di ogni magazzino
        capLeft[i] = cap[i];
    }

    for (var j = 0; j < n; j++) { //Per ogni elemento della soluzione, sottraggo la sua richiesta
        capLeft[solution[j]] -= req[solution[j]][j];
    }
    return capLeft;
}

/**
 * Perturbs the cost matrix given a percent of perturbation (every value will increased or decreased of x percent)
 * @param percent percent of perturbation
 * @returns {Array} perdurbed cost matrix
 */
function perturbation(percent) {
    var perturbed = new Array(m);
    for (var i = 0; i < m; i++) {
        perturbed[i] = new Array(n);
    }

    //Modifico la matrice dei costi del +-x%
    for (var j = 0; j < n; j++) {
        for (i = 0; i < m; i++) {
            if (Math.random() < 0.5) {
                perturbed[i][j] = c[i][j] + c[i][j] * percent / 100.0;
            } else {
                perturbed[i][j] = c[i][j] - c[i][j] * percent / 100.0;
            }
        }
    }
    return perturbed;
}

/**
 * Normalizes a matrix by column
 * @param array
 * @return {Array} a normalized array
 */
/*function normalizeByColumn(array) {
    array = arrayClone(array);
    for (var i = 0; i < n; i++) {
        var column = array.map((value) => value[i]);
        var minValue = Math.min.apply(null, column);
        var maxValue = Math.max.apply(null, column);

        for (var j = 0; j < m; j++) {
            array[j][i] = (array[j][i] - minValue) / (maxValue - minValue);
        }
    }
    return array;
}*/

/**
 * Normalizes an array between two values
 * @param array
 * @return {Array} a normalized array
 */
/*function normalizeByRow(array) {
    array = arrayClone(array);
    for (var i = 0; i < m; i++) {
        var row = array[i];
        var minValue = Math.min.apply(null, row);
        var maxValue = Math.max.apply(null, row);

        for (var j = 0; j < n; j++) {
            array[i][j] = (array[i][j] - minValue) / (maxValue - minValue);
        }
    }
    return array;
}

function normalizeAllMatrix(array) {
    var minValue = Number.MAX_VALUE;
    var maxValue = Number.MIN_VALUE;
    array = arrayClone(array);
    for (var i = 0; i < n; i++) {
        var row = array[i];
        minValue = (minValue > Math.min.apply(null, row)) ? Math.min.apply(null, row) : minValue;
        maxValue = (maxValue < Math.max.apply(null, row)) ? Math.max.apply(null, row) : maxValue;
        for (var j = 0; j < m; j++) {
            array[j][i] = (array[j][i] - minValue)/(maxValue - minValue);
        }
    }
    return array;
}*/