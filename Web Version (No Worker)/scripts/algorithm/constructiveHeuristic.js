/**
 * Solve the GAP problem with a simple constructive heuristic. The solution is saved in solBest and ZBest
 * @returns {{solution: *, cost}} an object containing the new solution and its cost
 */
function solveConstruct() {
    var startTime = new Date().getTime();
    if(verboseFlag) console.log("Starting at " + startTime);

    solBest = new Array(n);

    var dist = new Array(m); //Mappa  Richiesta - Magazzino (per ogni cliente)
    var capLeft = cap.slice();
    var z = 0;

    for (var i = 0; i < m; i++) {
        dist[i] = new Array(2);
    }

    //Riempio l'array dist
    for (var j = 0; j < n; j++) {
        for (i = 0; i < m; i++) {
            dist[i][0] = req[i][j];
            dist[i][1] = i;
        }
        //dist.sort((a, b) => a[0] - b[0]);
        dist.sort(function(a,b) {
            return a[0]-b[0];
        });

        for (i = 0; i < m; i++) {
            if (capLeft[dist[i][1]] >= dist[i][0]) {
                solBest[j] = dist[i][1];
                capLeft[dist[i][1]] -= dist[i][0];
                z += c[solBest[j]][j];
                break;
            }
        }
    }

    var time = new Date().getTime() - startTime;
    if(verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    if (checkFeasibility(solBest) != z) {
        alert("Error! The generated solution is not feasible!");
        solBest = undefined;
        zBest = -1;
    } else {
        zBest = z;
    }

    return {solution: solBest, cost: z, time: time};
}