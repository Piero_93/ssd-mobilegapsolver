/**
 * GRASP Heuristic
 * @param candidateListSize candidate list length
 * @param iterations max number of iteration
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function graspConstructive(candidateListSize, iterations, timeoutMillis) {
    if (verboseFlag) console.log("GRASP Constructive Heuristic");

    var startTime = new Date().getTime();
    if (verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec " + " with " + iterations + " iterations");

    var feasible = 0;
    var notFeasible = 0;

    var iteration = 0;
    do {
        iteration++;
        var solution = [];
        var capLeft = cap.slice();
        var z = 0;

        var dist = new Array(m); //Mappa  Richiesta - Magazzino (per ogni cliente)

        for (var i = 0; i < m; i++) {
            dist[i] = new Array(3);
        }

        //Per ogni cliente...
        for (var j = 0; j < n; j++) {
            //Costruisco il vettore dist e lo ordino per un criterio da me scelto
            for (i = 0; i < m; i++) {
                dist[i][0] = req[i][j];
                dist[i][1] = i;
                dist[i][2] = c[i][j]; //Criterio di scelta degli elementi della Candidate List
            }
            dist.sort(function (a, b) {
                return a[2] - b[2];
            })
            //dist.sort((a, b) => a[2] - b[2]);

            var candidateList = [];

            //Seleziono gli elementi da mettere nella candidate list
            for (i = 0; i < m; i++) {
                var current = dist[i][1];
                if (capLeft[current] >= req[current][j]) {
                    candidateList.push(current);
                    if (candidateList.length == candidateListSize) {
                        break;
                    }
                }
            }

            //Se ci sono candidati, ne estraggo uno a caso e lo sostituisco
            if (candidateList.length > 0) {
                var index = Math.floor(Math.random() * candidateList.length);
                solution[j] = candidateList[index];
                capLeft[solution[j]] -= req[solution[j]][j];
                z += c[solution[j]][j];
            } else {
                break;
            }
        }

        if (checkFeasibility(solution) == z) {
            if (verboseFlag) console.log("GRASP - it." + iteration + ", new feasible solution. z = " + z);

            //Eseguo anche la VND per ottimizzare ulteriormente il risultato
            var vndOut = variableNeighborhoodDescent(solution, timeoutMillis - (new Date().getTime() - startTime));
            if (vndOut.cost < z) {
                solution = arrayClone(vndOut.solution);
                z = vndOut.cost;
            }

            if (z < zBest) {
                zBest = z;
                solBest = arrayClone(solution);
            }

            feasible++;
        } else {
            //console.log("GRASP - it." + iteration + ", not feasible solution");
            notFeasible++;
        }

    } while (iteration < iterations && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if (verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    console.log("Feasible solution found: " + feasible);
    console.log("Not feasible solution found: " + notFeasible);

    return {solution: solution, cost: z, time: time, iteration: iteration };
}