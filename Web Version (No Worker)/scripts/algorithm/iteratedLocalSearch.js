/**
 * Iterated Local Search Optimization
 * @param solution the solution
 * @param percent perturbation percent
 * @param iterations max number of iteration
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function iteratedLocalSearch(solution, percent, iterations, timeoutMillis) {
    if(verboseFlag) console.log("Iterated Local Search - ILS");

    var startTime = new Date().getTime();
    if(verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec " + " with " + iterations + " iterations");

    //While(...)
    //OPT10 sui costi veri con costo zvero
    //Se zvero < zebest --> zbest = zvero
    //OPT10 sui costi perturbati --> va a finire in sol

    solution = arrayClone(solution);
    var z = checkFeasibility(solution);

    if (percent == undefined || percent < 0 || percent > 100) {
        percent = 10.0; //Numero compreso tra 0 e 100
    }

    var iteration = 0;
    do {
        //Eseguo l'ottimizzazione locale
        var resultOfLocalOpt = localOptimization10(solution);
        solution = resultOfLocalOpt.solution;
        z = resultOfLocalOpt.cost;

        //Se la soluzione trovata è migliore la salvo
        if (z < zBest) {
            zBest = z;
            solBest = arrayClone(solution);
        }

        //Eseguo l'ottimizzazione locale con costi perturbati
        resultOfLocalOpt = localOptimization10(solution, perturbation(percent));
        solution = resultOfLocalOpt.solution;
        z = checkFeasibility(solution); //Il costo va ricalcolato perchè i costi erano perturbati

        //Se la soluzione trovata è migliore la salvo
        if (z < zBest) {
            zBest = z;
            solBest = arrayClone(solution);
        }

        iteration++;
    } while (iteration < iterations && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if(verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");


    return {solution: solution, cost: z, time: time, iteration: iteration };
}