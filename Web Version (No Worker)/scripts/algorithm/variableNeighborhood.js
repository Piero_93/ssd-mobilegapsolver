/**
 * Variable Neighborhood Descent
 * @param solution the solution
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function variableNeighborhoodDescent(solution, timeoutMillis) {
    if(verboseFlag) console.log("Variable Neighborhood Descent - VND");

    var startTime = new Date().getTime();
    if(verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec ");

    solution = arrayClone(solution);
    var z = checkFeasibility(solution);

    var isImproved;

    var iteration = 0;

    do {
        isImproved = false;
        //Eseguo le due ottimizzazioni
        var opt10output = localOptimization10(arrayClone(solution)); //OPT10
        var opt11output = localOptimization11(opt10output.solution); //OPT11

        //Se il risultato è migliore del precedente sostituisco la nuova soluzione con la vecchia
        if (opt11output.cost < z) {
            isImproved = true;
            if(verboseFlag) console.log("VND, accepting a new solution. z = " + z + "->" + opt11output.cost);
            solution = opt11output.solution;
            z = opt11output.cost;
        }
        //Continuo fino a che le soluzioni migliorano oppure fino a quando finisce il tempo
        iteration++;
    } while (isImproved && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if(verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    if(z < zBest) {
        solBest = solution;
        zBest = z;
    }

    return {solution: solution, cost: z, time: time, iteration: iteration };
}

/**
 * Variable Neighborhood Search
 * @param solution the solution
 * @param iterations max number of iteration
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function variableNeighborhoodSearch(solution, iterations, timeoutMillis) {
    if(verboseFlag) console.log("Variable Neighborhood Search - VNS");

    var startTime = new Date().getTime();
    if(verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec " + " with " + iterations + " iterations");

    solution = arrayClone(solution);
    var z = checkFeasibility(solution);

    if (iterations == undefined) {
        iterations = 1000;
    }

    var iteration = 0;

    do {
        //Tento di migliorare la soluzione
        var vndOutput = variableNeighborhoodDescent(solution);

        //Se migliora sostituisco e riparto, altrimenti do uno scossone
        if (vndOutput.cost < z) {
            //Migliorata
            if(verboseFlag) console.log("VNS - it." + iteration + ", Sto accettando una nuova soluzione. z = " + vndOutput.cost + "->" + z);
            solution = arrayClone(vndOutput.solution);
            z = vndOutput.cost;
        } else {
            //Non migliorata
            //Modifico casualmente la soluzione (sullo stile di OTP-20)
            do {
                var changedSol = arrayClone(solution);

                var customer1 = Math.floor(Math.random() * n);
                var customer2 = Math.floor(Math.random() * n);
                var customer3 = Math.floor(Math.random() * n);

                changedSol[customer1] = Math.floor(Math.random() * m);
                changedSol[customer2] = Math.floor(Math.random() * m);
                changedSol[customer3] = Math.floor(Math.random() * m);
                var changedSolZ = checkFeasibility(changedSol);
            } while (changedSolZ == Number.MAX_VALUE);
            solution = arrayClone(changedSol);
            z = changedSolZ;
        }

        //Se è la soluzione migliore la salvo
        if (z < zBest) {
            solBest = arrayClone(solution);
            zBest = z;
        }

        iteration++;
    } while (iteration < iterations && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if(verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    return {solution: solution, cost: z, time: time, iteration: iteration };
}