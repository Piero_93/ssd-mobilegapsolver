/**
 * Simulated Annealing Optimization
 * @param solution the solution
 * @param T initial temperature
 * @param decreasing temperature difference between iterations
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function simulatedAnnealing(solution, T, decreasing, timeoutMillis) {
    if(verboseFlag) console.log("Simulated Annealing");

    var startTime = new Date().getTime();
    if(verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec ");

    solution = arrayClone(solution); //Genera una soluzione iniziale ammissibile S
    var z = checkFeasibility(solution);

    var k = 1.38 * Math.pow(10, -23); //Costante di Boltzmann
    if (T == undefined || T < 0) {
        T = 1000;
    }
    if (decreasing == undefined || decreasing < 0) {
        decreasing = 1;
    }
    var endingCondition = true;

    var iteration = 0;

    do {
        var newSol = arrayClone(solution);

        //Scelgo casualmente l'indice da modificare
        var i = Math.floor(Math.random() * n);
        var j = Math.floor(Math.random() * m);

        //Mettiamo il jesimo cliente nell'iesimo magazzino
        newSol[i] = j; //S'

        var newZ = checkFeasibility(newSol);
        if (newZ != Number.MAX_VALUE) { //Se la soluzione è ammissibile
            var probability = Math.exp(-(newZ - z) / (k * T));
            if (newZ < z || Math.random() < probability) { //Se la soluzione è migliore o comunque con una certa probabilità, la scelgo
                if(verboseFlag) console.log("Simulated Annealing - T=" + T + ", accepting a new solution. z = " + z + "->" + newZ);
                //Aggiorno la soluzione
                solution = arrayClone(newSol);
                z = newZ;
            }
        }

        //Se la soluzione attuale è migliore della migliore trovata fin'ora, la sostituisco
        if (z <= zBest) {
            solBest = arrayClone(solution);
            zBest = z;
        }

        //Diminuisco la temperatura
        T -= decreasing;

        //Aggiorno la ending condition
        endingCondition = (T <= 0);
        iteration++;
    } while (!endingCondition && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if(verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    return {solution: solution, cost: z, time: time, iteration: iteration };
}