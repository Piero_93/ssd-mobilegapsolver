console.log("Server starting...");
var port = 8080;
var connect = require('connect');
var serveStatic = require('serve-static');
var app = connect().use(serveStatic(__dirname)).listen(port);
console.log("Server ready.\nListening on port " + port + "...");

app.on('request', function (request, response) {
    if (request.url.toString().indexOf(".html") >= 0 || request.url.toString() === "/") {
        //console.log();
        console.log(request.method + " - " + "http://" + request.headers.host + "/" + request.url.substr(1));
    } else {
        console.log("\t\t\t" + request.url);
    }
});