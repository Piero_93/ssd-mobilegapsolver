var n; //customer number
var m; //server number
var c; //cost matrix
var req; //request matrix
var cap; //capacity vector

var solBest; //best stolution
var zBest = Number.MAX_VALUE; //cost of the best solution

var jInstance; //input instance

var verboseFlag = false;

//Limiter
var timeout = 60000000;//30000;
var iterations = 100000;//1000;

//Grasp
var candidateListSize = 5;

//ILS
var perturbationPercent = 10;

//Simulated Annealing
var initialTemperature = 1000;
var decreasing = 1;

//Tabu tenure
var tabuTenure = 10;