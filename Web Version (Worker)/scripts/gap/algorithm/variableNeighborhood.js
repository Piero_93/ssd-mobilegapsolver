/**
 * Variable Neighborhood Descent
 * PLEASE NOTE: Params are contained in bundle.params
 * @param solution the solution
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function variableNeighborhoodDescent(bundle) {
    var timeoutMillis = bundle.timeoutMillis;
    var solution = bundle.solution;

    if (verboseFlag) console.log("Variable Neighborhood Descent - VND");

    var startTime = new Date().getTime();
    if (verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec ");

    solution = arrayClone(solution);
    var z = checkFeasibility(solution);

    var isImproved;
    var iteration = 0;
    do {
        isImproved = false;
        //Eseguo le due ottimizzazioni
        var newBundle = JSON.parse(JSON.stringify(bundle));
        newBundle["solution"] = arrayClone(solution);
        var opt10Output = localOptimization10(bundle); //OPT10
        var opt11Output = localOptimization11(opt10Output); //OPT11

        //Se il risultato è migliore del precedente sostituisco la nuova soluzione con la vecchia
        if (opt11Output.cost < z) {
            isImproved = true;
            if (verboseFlag) console.log("VND, accepting a new solution. z = " + z + "->" + opt11Output.cost);
            solution = opt11Output.solution;
            z = opt11Output.cost;
        }
        //Continuo fino a che le soluzioni migliorano oppure fino a quando finisce il tempo
        iteration++;
    } while (isImproved && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if (verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    if (z < zBest) {
        bundle.solBest = solution;
        bundle.zBest = z;
    }

    bundle["solution"] = solution;
    bundle["cost"] = z;
    bundle["status"] = "success";
    bundle["doneIteration"] = iteration;
    bundle["totalIteration"] = iterations;
    bundle["timeout"] = timeoutMillis;
    bundle["totalTime"] = time;
    return bundle;
    return bundle;
    //return {solution: solution, cost: z, time: time};
}

/**
 * Variable Neighborhood Search
 * PLEASE NOTE: Params are contained in bundle.params
 * @param solution the solution
 * @param iterations max number of iteration
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function variableNeighborhoodSearch(bundle) {
    var timeoutMillis = bundle.timeoutMillis;
    var solution = bundle.solution;
    var iterations = bundle.iterations;

    if (verboseFlag) console.log("Variable Neighborhood Search - VNS");

    var startTime = new Date().getTime();
    if (verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec " + " with " + iterations + " iterations");

    solution = arrayClone(solution);
    var z = checkFeasibility(solution);

    if (iterations == undefined) {
        iterations = 1000;
    }
    
    var iteration = 0;
    do {
        //Tento di migliorare la soluzione
        var newBundle = JSON.parse(JSON.stringify(bundle));
        newBundle["solution"] = solution;
        newBundle["timeoutMillis"] = timeoutMillis - (new Date().getTime() - startTime);
        var vndOutput = variableNeighborhoodDescent(newBundle);


        //Se migliora sostituisco e riparto, altrimenti do uno scossone
        if (vndOutput.cost < z) {
            //Migliorata
            if (verboseFlag) console.log("VNS - it." + iteration + ", Sto accettando una nuova soluzione. z = " + vndOutput.cost + "->" + z);
            solution = arrayClone(vndOutput.solution);
            z = vndOutput.cost;
        } else {
            var changedSol;
            var changedSolZ;
            //Non migliorata
            //Modifico casualmente la soluzione (sullo stile di OTP-20)
            do {
                changedSol = arrayClone(solution);

                var customer1 = Math.floor(Math.random() * n);
                var customer2 = Math.floor(Math.random() * n);
                var customer3 = Math.floor(Math.random() * n);

                changedSol[customer1] = Math.floor(Math.random() * m);
                changedSol[customer2] = Math.floor(Math.random() * m);
                changedSol[customer3] = Math.floor(Math.random() * m);
                changedSolZ = checkFeasibility(changedSol);
            } while (changedSolZ === Number.MAX_VALUE);
            solution = arrayClone(changedSol);
            z = changedSolZ;
        }

        //Se è la soluzione migliore la salvo
        if (z < zBest) {
            bundle.solBest = arrayClone(solution);
            bundle.zBest = z;
        }

        iteration++;
    } while (iteration < iterations && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if (verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    bundle["solution"] = solution;
    bundle["cost"] = z;
    bundle["status"] = "success";
    bundle["doneIteration"] = iteration;
    bundle["totalIteration"] = iterations;
    bundle["timeout"] = timeoutMillis;
    bundle["totalTime"] = time;
    return bundle;
    //return {solution: solution, cost: z, time: time};
}