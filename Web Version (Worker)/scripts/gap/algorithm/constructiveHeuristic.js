/**
 * Solve the GAP problem with a simple constructive heuristic. The solution is saved in solBest and ZBest
 * @returns {{solution: *, cost}} an object containing the new solution and its cost
 */
function solveConstruct(bundle) {

    var startTime = new Date().getTime();
    if (verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + bundle.timeoutMillis + " msec " + " with " + iterations + " iterations");

    bundle.solBest = new Array(n);

    var dist = new Array(m); //Mappa  Richiesta - Magazzino (per ogni cliente)
    var capLeft = arrayClone(bundle.cap);

    var z = 0;

    for (var i = 0; i < m; i++) {
        dist[i] = new Array(2);
    }

    //Riempio l'array dist
    for (var j = 0; j < n; j++) {
        for (i = 0; i < m; i++) {
            dist[i][0] = bundle.req[i][j];
            dist[i][1] = i;
        }
        dist.sort(function (a, b) { return a[0] - b[0] });

        for (i = 0; i < m; i++) {
            if (capLeft[dist[i][1]] >= dist[i][0]) {
                bundle.solBest[j] = dist[i][1];
                capLeft[dist[i][1]] -= dist[i][0];
                z += c[bundle.solBest[j]][j];
                break;
            }
        }
    }

    var time = new Date().getTime() - startTime;
    if (verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");
    bundle["totalTime"] = time;

    if (checkFeasibility(bundle.solBest) !== z) {
        postMessage({ message: "Error! The generated solution is not feasible!" });
        bundle.solBest = undefined;
        bundle.zBest = -1;
        bundle["status"] = "fail";
    } else {
        bundle["status"] = "success";
        bundle.zBest = z;
    }

    return bundle;
}