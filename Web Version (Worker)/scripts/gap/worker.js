﻿importScripts("utilities.js",
    "globalvar.js",
    "gap.js",
    "algorithm/constructiveHeuristic.js",
    "algorithm/grasp.js",
    "algorithm/localOptimizations.js",
    "algorithm/variableNeighborhood.js",
    "algorithm/iteratedLocalSearch.js",
    "algorithm/simulatedAnnealing.js",
    "algorithm/tabuSearch.js");

function workerBody() {
    postMessage("I'm up!");
}

//e.data deve contenere tutte le variabili globali e nel campo .funcName il nome della funzione da chiamare
onmessage = function (e) {
    var data = JSON.parse(e.data);
    //postMessage(e.data);
    n = data.n;
    m = data.m;
    c = data.c;
    req = data.req;
    cap = data.cap;
    solBest = data.solBest;
    zBest = data.zBest;
    verboseFlag = data.verboseFlag;
    iterations = data.iterations;
    timeout = data.timeoutMillis;
    var result = eval(data.funcName)(data); //Chiama una funzione per nome
    postMessage(result);
}

//workerBody();

function executeFunctionByName(functionName, context, args) {
    //var args = [].slice.call(arguments).splice(2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
}