﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
$(document).ready(onDeviceReady);
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        //Handle backbutton event
        document.addEventListener("backbutton", goBack, true);

        //Cordova has been loaded. Perform any initialization that requires Cordova here.
        $("#in-iteration-number").val(localStorage.getItem("iterations"));
        $("#in-timeout-ms").val(localStorage.getItem("timeout"));
        $("#in-candidate-list-size").val(localStorage.getItem("candidateListSize"));
        $("#in-ils-perturbation").val(localStorage.getItem("perturbationPercent"));
        $("#in-initial-temperature").val(localStorage.getItem("initialTemperature"));
        $("#in-decreasing-value").val(localStorage.getItem("decreasing"));
        $("#in-tabu-tenure").val(localStorage.getItem("tabuTenure"));

        $("#btn-cancel").click(goBack);
        $("#btn-save").click(function () {
            var localIterations = $("#in-iteration-number").val();
            var localTimeout = $("#in-timeout-ms").val();
            var localPerturbation = $("#in-ils-perturbation").val();
            var localCandidateListSize = $("#in-candidate-list-size").val();
            var localInitialTemperature = $("#in-initial-temperature").val();
            var localDecreasing = $("#in-decreasing-value").val();
            var localTabuTenure = $("#in-tabu-tenure").val();

            if (localIterations < 0) {
                alert("Invalid value for Iterations. Using default value (1000)");
                localIterations = 100;
            }

            if (timeout < 1) {
                alert("Invalid value for Timeout. Using default value (30000)");
                localTimeout = 30000;
            }

            if (localPerturbation < 0 || localPerturbation > 100) {
                alert("Invalid value for Perturbation. Using default value (10)");
                localPerturbation = 10;
            }

            if (localCandidateListSize < 1) {
                alert("Invalid value for Candidate List Size. Using default value (5)");
                localCandidateListSize = 5;
            }

            if (localInitialTemperature < 1) {
                alert("Invalid value for Initial Temperature. Using default value (1000)");
                localInitialTemperature = 1000;
            }

            if (localDecreasing < 1) {
                alert("Invalid value for Decreasing Value. Using default value (1)");
                localInitialTemperature = 1;
            }

            if (localTabuTenure < 0) {
                alert("Invalid value for Tabu Tenure. Using default value (10)");
                localInitialTemperature = 10;
            }

            localStorage.setItem("iterations", localIterations);
            localStorage.setItem("timeout", localTimeout);
            localStorage.setItem("perturbation", localPerturbation);
            localStorage.setItem("candidateListSize", localCandidateListSize);
            localStorage.setItem("initialTemperature", localInitialTemperature);
            localStorage.setItem("decreasing", localDecreasing);
            localStorage.setItem("tabuTenure", localTabuTenure);

            alert("Saved");
            window.history.back();
        });
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

    function goBack() {
        alert("Exiting without saving");
        window.history.back();
        //navigator.app.exitApp();  // For Exit Application
    }