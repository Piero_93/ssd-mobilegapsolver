/**
 * Tabu Search Optimization
 * PLEASE NOTE: Params are contained in bundle.params
 * @param solution the solution
 * @param tabuTenure tabu list length
 * @param iterations max number of iteration
 * @param timeoutMillis max execution time
 * @returns {{solution: *, cost, time}} an object containing the new solution, its cost and the time consumed
 */
function tabuSearchOptimization(bundle) {
    var timeoutMillis = bundle.timeoutMillis;
    var solution = bundle.solution;
    var tabuTenure = bundle.funcParams[0];
    var iterations = bundle.iterations;


    if (verboseFlag) console.log("Tabu Search Optimization");

    var startTime = new Date().getTime();
    if (verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec " + " with " + iterations + " iterations");

    if (iterations == undefined) {
        iterations = 1000;
    }

    solution = arrayClone(solution);
    var z = checkFeasibility(solution);
    var capLeft = computeCapLeft(solution);

    var iSol;

    var tabuList = [];
    for (var i = 0; i < m; i++) {
        tabuList.push([]);
        tabuList[i].push(Number.MIN_VALUE);
    }

    var iteration = 0;
    do {
        var delta = 0;
        var jMin = 0;
        var iMin = 0;

        //Cerco la migliore soluzione nell'intorno (1-0)
        for (var j = 0; j < n; j++) {
            iSol = solution[j];
            for (i = 0; i < m; i++) {
                if (c[iSol][j] - c[i][j] > delta && //Se la nuova configurazione mi dà un costo più basso
                    capLeft[i] >= req[i][j] && //è ammissibile
                    iteration >= tabuList[i][j] + tabuTenure) { //e non è in tabu list
                    iMin = i;
                    jMin = j;
                    delta = c[iSol][j] - c[i][j];
                }
            }
        }

        //Aggiorno la soluzione con la migliore trovata
        iSol = solution[jMin];
        delta = c[iSol][jMin] - c[iMin][jMin];
        solution[jMin] = iMin;
        if (verboseFlag) console.log("Tabu Search - it." + iteration + ", accepting a new solution. z = " + z + "->" + (z - delta));

        //Aggiorno le capacità
        capLeft[iMin] -= req[iMin][jMin];
        capLeft[iSol] += req[iSol][jMin];

        //Aggiorno la Tabu List
        tabuList[iMin][jMin] = iteration; //metto il numero dell'iterazione in cui quella soluzione è stata usata
        z -= delta;

        if (z <= bundle.zBest) {
            bundle.solBest = arrayClone(solution);
            bundle.zBest = z;
        }

        iteration++;

    } while (iteration < iterations && ((new Date().getTime() - startTime < timeoutMillis) || timeoutMillis == undefined));
    var time = new Date().getTime() - startTime;
    if (verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    bundle["solution"] = solution;
    bundle["cost"] = z;
    bundle["status"] = "success";
    bundle["doneIteration"] = iteration;
    bundle["totalIteration"] = iterations;
    bundle["timeout"] = timeoutMillis;
    bundle["totalTime"] = time;
    return bundle;
    //return {solution: solution, cost: z, time: time};
}