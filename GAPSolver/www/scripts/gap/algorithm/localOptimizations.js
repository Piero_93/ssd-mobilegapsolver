/**
 * Local Optimization (1-0) of a solution
 * PLEASE NOTE: Params are contained in bundle.params
 * @param solution the solution
 * @param costMatrix the perturbed cost matrix (used in ILS). If not defined, the default matrix is used
 * @returns {{solution: *, cost}} an object containing the new solution and its cost
 */
function localOptimization10(bundle) { //OPT 10
    var costMatrix = bundle.costMatrix;
    var solution = bundle.solution;

    if (verboseFlag) console.log("Local Search 1-0");

    var startTime = new Date().getTime();
    if (verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec " + " with " + iterations + " iterations");

    if (costMatrix == undefined) { //Se viene passata una matrice dei costi perturbata usala, altrimenti usa quella normale
        costMatrix = arrayClone(c);
    }

    var z = checkFeasibility(solution); //Costo della nuova soluzione
    var capLeft = computeCapLeft(solution); //Capacità rimanenti per ogni magazzino

    var isImproved;
    do {
        isImproved = false;
        for (var j = 0; j < n; j++) { //Per tutti i clienti
            for (var i = 0; i < m; i++) { //Per tutti i magazzini
                if (costMatrix[i][j] < costMatrix[solution[j]][j] && capLeft[i] >= req[i][j]) {
                    //Se il costo scambiato è minore del costo attuale e se le capacità lo permettono
                    var oldZ = z;

                    //Aggiorno il costo
                    z -= costMatrix[solution[j]][j]; //Tolgo il costo vecchio
                    z += costMatrix[i][j]; //Aggiungo il costo nuovo

                    //Aggiorno la capacità
                    capLeft[solution[j]] += req[solution[j]][j]; //Rimetto la richiesta di prima (non più effettuata)
                    capLeft[i] -= req[i][j]; //Tolgo la richiesta di ora

                    //Aggiorno la soluzione
                    solution[j] = i; //Aggiorno la soluzione

                    //Stampo il risultato
                    if (verboseFlag) console.log("OPT10, new best solution. z = " + oldZ + "->" + z);
                    isImproved = true;
                    break;
                }
            }
            if (isImproved) {
                break;
            }
        }
    } while (isImproved);

    var time = new Date().getTime() - startTime;
    if (verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    bundle["solution"] = solution;
    bundle["cost"] = z;
    bundle["status"] = "success";
    bundle["totalTime"] = time;
    return bundle;
    //return {solution: solution, cost: z};
}


/**
 * Local Optimization (1-1) of a solution
 * @param solution the solution
 * @param costMatrix the perturbed cost matrix (used in ILS). If not defined, the default matrix is used
 * @returns {{solution: *, cost}} an object containing the new solution and its cost
 */
function localOptimization11(bundle) { //OPT 11
    var costMatrix = bundle.costMatrix;
    var solution = bundle.solution;

    if (verboseFlag) console.log("Local Search 1-1");

    var startTime = new Date().getTime();
    if (verboseFlag) console.log("Starting at " + startTime + " with timeout set to " + timeoutMillis + " msec " + " with " + iterations + " iterations");

    if (costMatrix == undefined) { //Se viene passata una matrice dei costi perturbata usala, altrimenti usa quella normale
        costMatrix = arrayClone(c);
    }

    var z = checkFeasibility(solution);
    var capLeft = computeCapLeft(solution);

    var isImproved = false;

    do {
        isImproved = false;
        for (var j1 = 0; j1 < n; j1++) {
            for (var j2 = 0; j2 < n; j2++) {
                var delta = costMatrix[solution[j1]][j1] + costMatrix[solution[j2]][j2]; //Costo attuale di J1 e J2
                var delta2 = costMatrix[solution[j2]][j1] + costMatrix[solution[j1]][j2]; //Costo futuro di J1 e J2

                if (capLeft[solution[j1]] + req[solution[j1]][j1] >= req[solution[j1]][j2] &&
                    capLeft[solution[j2]] + req[solution[j2]][j2] >= req[solution[j2]][j1] && //Se le capacità lo permettono
                    delta2 < delta) { //e il costo della nuova soluzione è minore
                    var oldZ = z;

                    //Aggiorno il costo
                    z = z - delta + delta2;

                    //Aggiorno le capacità
                    capLeft[solution[j1]] += req[solution[j1]][j1];
                    capLeft[solution[j1]] -= req[solution[j1]][j2];

                    capLeft[solution[j2]] += req[solution[j2]][j2];
                    capLeft[solution[j2]] -= req[solution[j2]][j1];

                    //Inverto gli elementi della soluzione
                    var aux = solution[j2];
                    solution[j2] = solution[j1];
                    solution[j1] = aux;

                    //Stampo il risultato
                    if(verboseFlag) console.log("OPT11, new best solution. z = " + oldZ + "->" + z);
                    isImproved = true;
                    break;
                }
            }
            if (isImproved) {
                break;
            }
        }
    } while (isImproved);

    var time = new Date().getTime() - startTime;
    if (verboseFlag) console.log("Ending at " + new Date().getTime() + " in " + time + " msec");

    bundle["solution"] = solution;
    bundle["cost"] = z;
    bundle["status"] = "success";
    bundle["totalTime"] = time;
    return bundle;
    //return {solution: solution, cost: z};
}