var url = 'http://astarte.csr.unibo.it/gapdata/';
var request;

function httpRequest(value) {
    solBest = undefined;
    zBest = Number.MAX_VALUE;
    if (value == undefined || value == "") {
        value = document.getElementById("dropDown").value;
        if (value === "") {
            value = prompt("Insert the file name and path on \"http://astarte.csr.unibo.it/gapdata/\":", "");
        }
    }
    console.log(url + value);
    request = new XMLHttpRequest();
    request.open('GET', url + value, true);
    request.onreadystatechange = handler;
    request.send();
}

function handler() {
    if (request.readyState === 4)
        if (request.status === 200)
            outputResult();
        else
            alert("Error\n   Status: " + request.status);
}

function outputResult() {
    var response = request.responseText;
    var textDiv = document.getElementById("textDiv");
    jInstance = JSON.parse(response);
    setInstance(jInstance);
}

function setInstance(jInstance) {
    m = jInstance.numfacilities;
    n = jInstance.numcustomers;
    c = jInstance.cost;
    req = jInstance.req;
    cap = jInstance.cap;
    alert("Obtained instance \"" + jInstance.name + "\"");
    $("#txt-current-instance").text("Currently loaded: " + jInstance.name);
}