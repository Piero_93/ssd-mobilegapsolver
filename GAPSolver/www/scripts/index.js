﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);

        //Cordova has been loaded. Perform any initialization that requires Cordova here.
        //$("#btn-test").click(launchWorker);

        refreshFromLocalStorage();

        //CONNECTING BUTTONS TO THEIR OPERATIONS
        //Load
        $("#btn-load").click({ address: $("#dropDown").value }, resetAndLoad);

        //Settings
        $("#btn-settings").click(openSettings);

        //Show
        $("#btn-show-instance-data").click(showCurrentInstanceData);
        $("#btn-show-current-best-cost").click(showCurrentBestCost);
        $("#btn-show-current-best-solution").click(showCurrentBestSolution);

        //Constructive
        $("#btn-solve-with-constructive").click({ funcName: "solveConstruct" }, launchWorker);
        $("#btn-solve-with-grasp").click({ funcName: "graspConstructive", funcParams: [candidateListSize] }, launchWorker);

        //Local Optimization
        $("#btn-local-opt-10").click({ funcName: "localOptimization10" }, launchWorker);
        $("#btn-local-opt-11").click({ funcName: "localOptimization11" }, launchWorker);
        $("#btn-ils").click({ funcName: "iteratedLocalSearch", funcParams: [perturbationPercent] }, launchWorker);

        //Variable Neighborhood
        $("#btn-vns").click({ funcName: "variableNeighborhoodSearch" }, launchWorker);
        $("#btn-vnd").click({ funcName: "variableNeighborhoodDescent" }, launchWorker);

        //Simulated Annealing
        $("#btn-simulated-annealing").click({ funcName: "simulatedAnnealing", funcParams: [initialTemperature, decreasing] }, launchWorker);

        //Tabu Search
        $("#btn-tabu-search").click({ funcName: "tabuSearchOptimization", funcParams: [tabuTenure] }, launchWorker);

        $(".spinner").hide();
    };

    function onPause() {
    };

    function onResume() {
    };

    /**
     * Create a worker that will call the function with the name specified in "funcName", using the parameters specified in "funcParams"
     * @param {} event contains the name of the function to be called and the parameters to be passed in "funcName" (String) and "funcParams" (Array) fields
     */
    function launchWorker(event) {
        var funcName = event.data.funcName;
        var funcParams = event.data.funcParams;

        if (jInstance == undefined) { //Cannot launch anything if an instance is not loaded
            alert("Please load an instance before hitting this button");
            return;
        } else if ((funcName !== "solveConstruct" && funcName !== "graspConstructive") &&
            (solBest == undefined || checkFeasibility(solBest) === Number.MAX_VALUE)) { //Cannot work on a solution if it has not been calculated yet
            alert("Please build a solution before hitting this button");
            return;
        }

        //Showing the working animation
        $(".spinner").show();
        
        //Checking the existence of workers in the environment
        if (typeof (Worker) !== "undefined") {
            //Web worker are supported

            //Creates the worker
            var w = new Worker("scripts/gap/worker.js");

            //Pass the global environment, the function name and the function params to the worker
            w.postMessage(JSON.stringify({
                n: n,
                m: m,
                c: c,
                req: req,
                cap: cap,
                solBest: solBest,
                zBest: zBest,
                solution: solBest,
                verboseFlag: false,
                iterations: iterations,
                timeoutMillis: timeout,
                funcName: funcName,
                funcParams: funcParams
            }));

            //Listen for messages from the worker
            w.onmessage = function (event) {
                var messageContent = event.data;

                if (messageContent != undefined &&
                    messageContent.status != undefined &&
                    messageContent.status === "success") { //If the computation succeeded

                    $(".spinner").hide();

                    if (messageContent.zBest <= zBest) {
                        //Update the global best solution
                        solBest = messageContent.solBest;
                        zBest = messageContent.zBest;
                    }
                    if (messageContent.cost <= zBest) {
                        solBest = messageContent.solution;
                        zBest = messageContent.cost;
                    }

                    //Show the result
                    var message = "Algoritm ended with success. Current best cost is: " + zBest;
                    message += messageContent.doneIteration !== undefined ? ".\nDone " + messageContent.doneIteration + " iterations of " + messageContent.totalIteration : "";
                    message += messageContent.totalTime !== undefined ? ".\nTook " + messageContent.totalTime + " msec" : "";
                    message += messageContent.timeout !== undefined ? " out of " + messageContent.timeout + " msec" : "";
                    message += ".";
                    alert(message);

                } else if (messageContent != undefined &&
                    messageContent.status != undefined &&
                    messageContent.status === "fail") { //The computation did not succeed
                    alert("Error!");

                } else if (messageContent != undefined && messageContent.message != undefined) {
                    alert(messageContent.message);

                } else {
                    alert("OTHER MESSAGE: " + JSON.stringify(messageContent));
                }

            }
        } else {
            //Web worker are not supported
            alert("Sorry, this app is not compatible with your device");
        }
    }

    /**
     * Reset the current global environment and load a new instance from internet
     * @param {} event contains the address to be loaded (missing part after "http://astarte.csr.unibo.it/gapdata/")
     */
    function resetAndLoad(event) {
        zBest = Number.MAX_VALUE;
        solBest = undefined;
        jInstance = undefined;
        localStorage.clear();
        httpRequest(event.data.address);
        candidateListSize = 80 / 100 * m < 5 ? 5 : 80 / 100 * m;
    }

    /**
     * Show some information about the current instance
     */
    function showCurrentInstanceData() {
        if (jInstance != undefined) {
            alert("Current instance is: \"" + jInstance.name + "\" \n" +
                "\t\t- Number of customers: " + jInstance.numcustomers + "\n" +
                "\t\t- Number of facilities: " + jInstance.numfacilities);
        } else {
            alert("Please load a JSON before hitting this button");
        }
    }

    /**
     * Show the current best cost
     */
    function showCurrentBestCost() {
        if (zBest != undefined && zBest !== Number.MAX_VALUE) {
            alert("Current best cost is: " + zBest);
        } else {
            alert("Please build a solution before hitting this button");
        }
    }

    /**
     * Show the current best solution
     */
    function showCurrentBestSolution() {
        if (solBest != undefined && zBest !== Number.MAX_VALUE) {
            alert("Current best solution is: " + solBest);
        } else {
            alert("Please build a feasible solution before hitting this button");
        }
    }

    /**
     * Open the setting page
     */
    function openSettings() {
        //Update the localStorace to remember the values already calculated
        localStorage.setItem("solBest", solBest);
        localStorage.setItem("zBest", zBest);
        localStorage.setItem("jInstance", JSON.stringify(jInstance));

        localStorage.setItem("iterations", iterations);
        localStorage.setItem("timeout", timeout);
        localStorage.setItem("perturbationPercent", perturbationPercent);
        localStorage.setItem("candidateListSize", candidateListSize);
        localStorage.setItem("initialTemperature", initialTemperature);
        localStorage.setItem("decreasing", decreasing);
        localStorage.setItem("tabuTenure", tabuTenure);

        //Open the page
        window.location = "settings.html";

        localStorage.setItem("first", false);
    }

    /**
     * Refresh the data from the local storage (if not present, set them to the default value)
     */
    function refreshFromLocalStorage() {
        solBest = localStorage.getItem("solBest") != undefined ? localStorage.getItem("solBest") : undefined;

        zBest = localStorage.getItem("zBest") != undefined ? localStorage.getItem("zBest") : Number.MAX_VALUE;

        jInstance = (localStorage.getItem("jInstance") !== "undefined" && localStorage.getItem("jInstance") != null) ? JSON.parse(localStorage.getItem("jInstance")) : undefined;
        if (jInstance != undefined) {
            n = jInstance.numcustomers;
            m = jInstance.numfacilities;
            c = jInstance.cost;
            req = jInstance.req;
            cap = jInstance.cap;
            $("#txt-current-instance").text("Currently loaded: " + jInstance.name);
        }

        iterations = localStorage.getItem("iterations") != null ? localStorage.getItem("iterations") : Number.MAX_VALUE;

        timeout = localStorage.getItem("timeout") != null ? localStorage.getItem("timeout") : 30000;

        perturbationPercent = localStorage.getItem("perturbationPercent") != null ? localStorage.getItem("perturbationPercent") : 10;

        candidateListSize = localStorage.getItem("candidateListSize") != null ? localStorage.getItem("candidateListSize") : 5;

        initialTemperature = localStorage.getItem("initialTemperature") != null ? localStorage.getItem("initialTemperature") : 1000;

        decreasing = localStorage.getItem("decreasing") != null ? localStorage.getItem("decreasing") : 1;

        tabuTenure = localStorage.getItem("tabuTenure") != null ? localStorage.getItem("tabuTenure") : 5;

        localStorage.clear();
    }

})();